from api import AhrefsApi
try: import simplejson as json
except ImportError: import json
from pprint import pprint
import csv
from multiprocessing import Pool as ThreadPool

with open('shophouzzurllist', 'r') as f:
    start_urls = [url.strip() for url in f.readlines()]

def write_csv(*args):
    with open('backlinks_short.csv', 'ab') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow([args])
        print args

def multi_refdomains_by_type(url):
    api = AhrefsApi('http://apiv2.ahrefs.com', '45b65314ca8c15862013401956afcb87eece798f')
    ahrefs_refdomains = api.refdomains_by_type(url, mode='prefix').get()
    try:
        new_total = (ahrefs_refdomains['stats']['refdomains'])
    except:
        new_total = 0
    write_csv(url,new_total)

p = ThreadPool(5)
p.map(multi_refdomains_by_type, start_urls)